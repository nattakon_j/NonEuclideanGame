using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllnewWaterLogic : MonoBehaviour
{
    public int waterSerial;
    public enum direction { None, Forward, Backward, Left, Right };

    [SerializeField] direction flowDirection;

    public enum BlockState { None, Forward, Backward, Left, Right, All};

    [SerializeField] BlockState blockState;

    [SerializeField] LayerMask layertoblock;

    [SerializeField] GameObject hitOBJ;

    [SerializeField] private GameObject waterPrefab;

    public int waterValue;
    AllnewWaterLogic otherWater;

    [SerializeField] bool checkallow;

    float Delay = 1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        Raycheck();
    }

    IEnumerator WaterGenarating(GameObject waterReference, Vector3 waterPosition)
    {
        yield return new WaitForSeconds(0.5f);
        GameObject waterSpawn = Instantiate(waterReference, waterPosition, Quaternion.identity);
        waterSpawn.GetComponent<AllnewWaterLogic>().waterSerial = waterSerial;
    }

    void Raycheck()
    {
        if (!checkallow)
        {
            return;
        }

        int count = 0;

        if (Delay <= 0)
        {
            if (Physics.Raycast(gameObject.transform.position, -transform.up, out RaycastHit downHit, 1.25f, layertoblock))
            {

                if (Physics.Raycast(gameObject.transform.position, transform.forward, out RaycastHit forwardHit, 2.5f, layertoblock))
                {
                    hitOBJ = forwardHit.transform.gameObject;
                    blockState = BlockState.Forward;
                    count += 1;
                    OBJcheck();
                }
                else
                {
                    flowDirection = direction.Forward;
                    if (Delay < 0)
                    {
                        StartCoroutine(WaterGenarating(waterPrefab, transform.position + new Vector3(0, 0, 5)));
                        Delay = 1.0f;
                    }
                }

                if (Physics.Raycast(gameObject.transform.position, -transform.forward, out RaycastHit backwardHit, 2.5f, layertoblock))
                {
                    hitOBJ = backwardHit.transform.gameObject;
                    blockState = BlockState.Backward;
                    count += 1;
                    OBJcheck();
                }
                else
                {
                    flowDirection = direction.Backward;
                    if (Delay < 0)
                    {
                        StartCoroutine(WaterGenarating(waterPrefab, transform.position + new Vector3(0, 0, 5)));
                        Delay = 1.0f;
                    }
                }

                if (Physics.Raycast(gameObject.transform.position, transform.right, out RaycastHit rightHit, 2.5f, layertoblock))
                {
                    hitOBJ = rightHit.transform.gameObject;
                    blockState = BlockState.Right;
                    count += 1;
                    OBJcheck();
                }
                else
                {
                    flowDirection = direction.Right;
                    if (Delay < 0)
                    {
                        StartCoroutine(WaterGenarating(waterPrefab, transform.position + new Vector3(0, 0, 5)));
                        Delay = 1.0f;
                    }
                }

                if (Physics.Raycast(gameObject.transform.position, -transform.right, out RaycastHit leftHit, 2.5f, layertoblock))
                {
                    hitOBJ = leftHit.transform.gameObject;
                    blockState = BlockState.Left;
                    count += 1;
                    OBJcheck();
                }
                else
                {
                    flowDirection = direction.Left;
                    if (Delay < 0)
                    {
                        StartCoroutine(WaterGenarating(waterPrefab, transform.position + new Vector3(0, 0, 5)));
                        Delay = 1.0f;
                    }
                }
            }
            else
            {
                StartCoroutine(WaterGenarating(waterPrefab, transform.position + new Vector3(0, -2.5f, 0)));
                flowDirection = direction.None;
                Delay = 1.0f;
            }
        }
        else
        {
            Delay -= Time.deltaTime;
        }

        if (count == 4)
        {
            blockState = BlockState.All;
        }
    }

    void OBJcheck()
    {
        if (hitOBJ != null)
        {
            if (hitOBJ.tag == "Water")
            {
                if (hitOBJ.GetComponent<AllnewWaterLogic>())
                {
                    AllnewWaterLogic watertocompare = hitOBJ.GetComponent<AllnewWaterLogic>();
                    if (waterSerial != watertocompare.waterSerial)
                    {
                        if ((watertocompare.waterValue < waterValue + 1) && (otherWater != watertocompare))
                        {
                            watertocompare.waterValue += 1;
                            otherWater = watertocompare;
                        }
                    }
                    else
                    {
                        if (watertocompare.waterValue < waterValue)
                        {
                            watertocompare.waterValue = waterValue;
                        }
                    }
                }
            }
        }
        else
        {
            blockState = BlockState.None;
        }
    }
}
