using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPPACK : MonoBehaviour
{
    [SerializeField] PlayerBasics playerBasics;
    // Start is called before the first frame update
    void Start()
    {
        playerBasics = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerBasics>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            playerBasics.Setdamage(-5);
            Destroy(this.gameObject);
        }
    }

}

