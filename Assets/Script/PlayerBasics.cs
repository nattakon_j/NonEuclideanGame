using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBasics : MonoBehaviour
{
    [SerializeField] int HP;
    [SerializeField] GameManagerTEMP gm;
    
    public int _HP => HP;
    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("manager").GetComponent<GameManagerTEMP>();
    }

    // Update is called once per frame
    void Update()
    {
        if(HP <= 0)
        {
            gm.losing();
        }
        if(HP > 100)
        {
            HP = 100;
        }

        if(this.gameObject.transform.position.y < -1)
        {
            this.gameObject.transform.position = new Vector3(0, 0, 0);
        }
    }

    public void Setdamage(int damage)
    {
        HP -= damage;
    }

    
}
