using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuiscideBomber : Basic_Enemy
{
    [SerializeField] GameObject ExplosionEffect;
    [SerializeField] bool testchasing;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Chase();

        if( HP <= 0)
        {
            //Debug.Log("Incondition");
            explode();
        }
    }

    private void Chase()
    {
        if (playeriInTrigger._PlayerinTrigger)
        {
            transform.LookAt(PlayerRef.transform);
            agent.SetDestination(PlayerRef.transform.position);

            if(Vector3.Distance(PlayerRef.transform.position, transform.position) < Detecradius / 4)
            {
                explode();
            }
        }
    }

    private void explode()
    {
        if (testchasing)
        {
            return;
        }
        Collider[] objectsInRange = Physics.OverlapSphere(transform.position, Detecradius / 3, Targetmask);
        if (objectsInRange.Length != 0)
        {
            PlayerRef.GetComponent<PlayerBasics>().Setdamage(damage);
            Instantiate(ExplosionEffect, transform.position, transform.rotation);
            death();
        }
        else
        {
            Instantiate(ExplosionEffect, transform.position, transform.rotation);
            death(); 
        }
    }
}
