using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Movement : MonoBehaviour
{

    CharacterController cc;
    bool _isGrounded;
    private Vector2 _inputMovement;
    private Vector3 _move;
    float _currentSpeed;
    float ySpeed;
    float original;
    [SerializeField] Transform _cameraTransform;
    [SerializeField] float Runspeed;
    [SerializeField] float Normalspeed;
    [SerializeField] float JumpForce;
    [SerializeField] float Glide;
    [SerializeField] float AirMultiplier;
    [SerializeField] float AirdashSPD;
    [SerializeField] int Airdash;
    [SerializeField] int jumpcount;
    int jc;

    int ac;
    bool isrun;
    // Start is called before the first frame update
    void Start()
    {
        cc = GetComponent<CharacterController>();
        original = cc.stepOffset;
        jc = jumpcount;
        ac = Airdash;
    }

    // Update is called once per frame
    void Update()
    {
        _isGrounded = cc.isGrounded;
        if (_isGrounded && ySpeed < 0)
        {
            ySpeed = -0.5f;
            cc.stepOffset = original;
        }
        else
        {
            cc.stepOffset = 0f;
        }

        

        _inputMovement = InputManager.instance.WASD;
        _move = new Vector3(_inputMovement.x, 0f, _inputMovement.y);
        _move = _cameraTransform.forward * _move.z + _cameraTransform.right * _move.x;

        ySpeed += Physics.gravity.y * Time.deltaTime;
        
        
        if (InputManager.instance.Spacebar && jumpcount > 0)
        {
            jumpcount -= 1;
            ySpeed = JumpForce;
            
        }

        if (_isGrounded)
        {
            jumpcount = jc;
            Airdash = ac;
        }

        _move.y = ySpeed;
        

        if (InputManager.instance.Shift == 1 && _inputMovement.y > 0)
        {
            _currentSpeed = Runspeed;
        }
        else
        {
            _currentSpeed = Normalspeed;
        }


        if (!_isGrounded)
        {
            _move.y = ySpeed / Glide;
            if(InputManager.instance.Shift == 1 && Airdash > 0)
            {
                Airdash -= 1;
                _move.x = _move.x * (AirMultiplier * AirdashSPD);
                _move.z = _move.z * (AirMultiplier * AirdashSPD);
            }
            else
            {
                _move.x = _move.x * AirMultiplier;
                _move.z = _move.z * AirMultiplier;
            }
            
        }
        _move = _move.normalized * _currentSpeed * Time.deltaTime;
        cc.Move(_move);




    }

}
