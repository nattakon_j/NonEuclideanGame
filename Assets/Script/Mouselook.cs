using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouselook : MonoBehaviour
{
    Vector2 mouse;
    [SerializeField]Transform player;
    [SerializeField] float mouseSensitivity;

    float RotOnX;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        mouse = InputManager.instance.MLook;
        float mouseY = mouse.y * Time.deltaTime * mouseSensitivity;
        float mouseX = mouse.x * Time.deltaTime * mouseSensitivity;


        RotOnX -= mouseY;
        RotOnX = Mathf.Clamp(RotOnX, -85f, 85f);
        transform.localEulerAngles = new Vector3(RotOnX, 0, 0);

        player.Rotate(Vector3.up * mouseX);
    }
}
