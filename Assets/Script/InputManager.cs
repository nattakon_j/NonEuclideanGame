using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    private static InputManager _instance;
    public static InputManager instance => _instance;

    public PlayerCon _inputMaster;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        _inputMaster = new PlayerCon();
    }

    private void OnEnable()
    {
        _inputMaster.Enable();
    }
    private void OnDisable()
    {
        _inputMaster.Disable();
    }

    public Vector2 WASD => _inputMaster.Player.HorizontalMovement.ReadValue<Vector2>();

    public Vector2 MLook => _inputMaster.Player.mouseLook.ReadValue<Vector2>();
    public bool Spacebar => _inputMaster.Player.Jump.triggered;

    public float Shift => _inputMaster.Player.Sprint.ReadValue<float>();

    public float Ctrl => _inputMaster.Player.Crouch.ReadValue<float>();
    public float LeftClick => _inputMaster.Player.Shoot.ReadValue<float>();

}
