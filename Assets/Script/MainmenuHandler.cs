using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainmenuHandler : MonoBehaviour
{
    [SerializeField] Savor savor;

    [SerializeField] Button stage1;
    [SerializeField] Text S1Text;
    [SerializeField] Button stage2;
    [SerializeField] Text S2Text;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        savor = GameObject.FindGameObjectWithTag("save").GetComponent<Savor>();
    }

    // Update is called once per frame
    void Update()
    {
        if (savor.save.SaveDATA[0].unlocked)
        {
            stage1.interactable = true;
        }
        else
        {
            stage1.interactable = false;
        }

        if (savor.save.SaveDATA[1].unlocked)
        {
            stage2.interactable = true;
        }
        else
        {
            stage2.interactable = false;
        }

        S1Text.text = "Stage : " + savor.save.SaveDATA[0].stage + "\n" + "Unlocked : " + savor.save.SaveDATA[0].unlocked + "\n" + "Best time survived : " + savor.save.SaveDATA[0].survivetime + " Sec" + "\n" + "Best Kill count : " + +savor.save.SaveDATA[0].Killcount;
        S2Text.text = "Stage : " + savor.save.SaveDATA[1].stage + "\n" + "Unlocked : " + savor.save.SaveDATA[1].unlocked + "\n" + "Best time survived : " + savor.save.SaveDATA[1].survivetime + " Sec" + "\n" + "Best Kill count : " + +savor.save.SaveDATA[1].Killcount;
    }

    public void loadscene(int scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void exit()
    {
        savor.SaveB4Exit();
        Application.Quit();
    }
}
