using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    [SerializeField] Transform _player;
    [SerializeField] Transform HitObj;
    [SerializeField] Transform _Reciever;
    [SerializeField] bool _PlayerOverlap = false;
    // Update is called once per frame
    void Update()
    {
        if (_PlayerOverlap)
        {
            Vector3 _portalToPlayer = HitObj.position - transform.position;
            float _dotprod = Vector3.Dot(transform.up, _portalToPlayer);

            if(_dotprod < 0)
            {
                
                float _rotDiff = -Quaternion.Angle(transform.rotation, _Reciever.rotation);
                //_rotDiff += 180;
                HitObj.Rotate(Vector3.up, _rotDiff);

                Vector3 _Posoff = Quaternion.Euler(0f, _rotDiff, 0f) * _portalToPlayer;

                HitObj.position = _Reciever.position + _Posoff;

                HitObj = null;

                _PlayerOverlap = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            HitObj = other.transform;
            _PlayerOverlap = true;
            
        }

        //Debug.Log(other.name);
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other != null)
        {
            _PlayerOverlap = false;
        }
    }
}
