using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeAttacker : Basic_Enemy
{
    [SerializeField] GameObject Bullet;
    [SerializeField] GameObject shootPoint;
    [SerializeField] GameObject ShootingBullet;
    [SerializeField] float BulletSpeed;
    [SerializeField] float RateOfFire;
    [SerializeField] bool onfiring;
    float _delay;
    // Start is called before the first frame update
    void Start()
    {
        _delay = RateOfFire;
    }

    // Update is called once per frame
    void Update()
    {
        Chase();
        if (onfiring)
        {
            RateOfFire -= Time.deltaTime;
            if (RateOfFire <= 0)
            {
                
                    ShootingBullet = Instantiate(Bullet, shootPoint.transform.position, shootPoint.transform.rotation);
                    ShootingBullet.GetComponent<Rigidbody>().AddForce(ShootingBullet.transform.forward * BulletSpeed, ForceMode.Impulse);
                    RateOfFire = _delay;
                
            }
        }

        if(HP <= 0)
        {
            death();
        }
        
    }

    private void Chase()
    {
        if (playeriInTrigger._PlayerinTrigger)
        {
            transform.LookAt(PlayerRef.transform);
            agent.SetDestination(PlayerRef.transform.position);

            if (Vector3.Distance(PlayerRef.transform.position, transform.position) < Detecradius / 1.47)
            {
                onfiring = true;
                agent.velocity = Vector3.zero;
            }
            else
            {
                onfiring = false;
            }
        }
        else
        {
            onfiring = false;
        }
    }
}
