using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SaveData", menuName = "ScriptableObjects/Save", order = 1)]
public class Save : ScriptableObject
{
    [System.Serializable]
    public class data
    {
        public int stage;
        public float survivetime;
        public int Killcount;
        public float Timeneeded;
        public bool unlocked;
    }

    [SerializeField]public data[] SaveDATA;
}
