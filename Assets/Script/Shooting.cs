using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    [SerializeField] GameObject bullet;
    [SerializeField] GameObject gun;
    [SerializeField] float Delay;
    [SerializeField] float bulletspeed;
    [SerializeField] GameObject ShootingBullet;
    [SerializeField] GameManagerTEMP gm;
    [SerializeField] float _delay;
    float _BD;
    int tempt = 0;
    // Start is called before the first frame update
    void Start()
    {
        _delay = Delay;
        _BD = Delay;
        gm = GameObject.FindGameObjectWithTag("manager").GetComponent<GameManagerTEMP>();
    }

    // Update is called once per frame
    void Update()
    {
        Delay -= Time.deltaTime;
        if(Delay <= 0)
        {
            if (InputManager.instance.LeftClick > 0)
            {
                ShootingBullet = Instantiate(bullet, gun.transform.position, gun.transform.rotation);
                ShootingBullet.GetComponent<Rigidbody>().AddForce(ShootingBullet.transform.forward * bulletspeed, ForceMode.Impulse);
                Delay = _delay;
            }
        }

        

        if (tempt != gm.killcount)
        {
            
            _delay = _BD - (2 * ((float)gm.killcount / 1000f));
            tempt = gm.killcount;
            if(_delay < 0.001)
            {
                _delay = 0.001f;
            }
        }
          
    }

}
