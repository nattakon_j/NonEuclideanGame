

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] bool friendly;
    [SerializeField] int Damage;
    [SerializeField] GameManagerTEMP gm;
    float bulletlifetime = 10f;
    // Start is called before the first frame update
    void Start()
    {
        if (friendly)
        {
            Physics.IgnoreLayerCollision(3, 6);
        }
        else
        {
            Physics.IgnoreLayerCollision(7, 8);
        }
        
        

        

        gm = GameObject.FindGameObjectWithTag("manager").GetComponent<GameManagerTEMP>();

        if (friendly)
        {
            Damage = Damage + (int) Mathf.Floor(gm.killcount / 10);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        bulletlifetime -= Time.deltaTime;

        if(bulletlifetime <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (friendly)
        {
            if(other.tag == "Enemy")
            {
                if (other.gameObject.GetComponent<SuiscideBomber>())
                {
                    other.gameObject.GetComponent<SuiscideBomber>().Setdamage(Damage);
                }
                else if (other.gameObject.GetComponent<RangeAttacker>())
                {
                    other.gameObject.GetComponent<RangeAttacker>().Setdamage(Damage);
                }
            }
        }
        else
        {
            if (other.tag == "Player")
            {
                if (other.gameObject.GetComponent<PlayerBasics>())
                {
                    other.gameObject.GetComponent<PlayerBasics>().Setdamage(Damage);
                }
            }
        }
        Destroy(this.gameObject);
    }
}
