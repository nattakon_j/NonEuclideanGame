using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayeriInTrigger : MonoBehaviour
{
    [SerializeField]bool PlayerIntrigger;

    public bool _PlayerinTrigger => PlayerIntrigger;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerIntrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerIntrigger = false;
        }
    }
}
