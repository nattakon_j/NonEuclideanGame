using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManagerTEMP : MonoBehaviour
{
    [SerializeField] int stage;
    [SerializeField] Text PlayerHP;
    [SerializeField] Text TimerText;
    [SerializeField] Text killcountText;
    [SerializeField] Text EndingText;
    [SerializeField] PlayerBasics pb;
    [SerializeField] float timer = 0;
    [SerializeField] Savor savedata;
    public float _timer => timer;
    public int killcount;

    // Start is called before the first frame update
    void Start()
    {
        savedata = GameObject.FindGameObjectWithTag("save").GetComponent<Savor>();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        PlayerHP.text = "HP : " + pb._HP;

        killcountText.text = "Kill Count : " + killcount;

        TimerText.text = "Time Survived : " + (int)Mathf.Floor(timer) + " sec";
        
    }

    public void losing()
    {
        for (int i = 0; i < savedata.save.SaveDATA.Length; i++)
        {
            if(stage == savedata.save.SaveDATA[i].stage)
            {
                if(timer > savedata.save.SaveDATA[i].survivetime)
                {
                    Debug.Log("1");
                    savedata.save.SaveDATA[i].survivetime = timer;
                }

                if (killcount > savedata.save.SaveDATA[i].Killcount)
                {
                    Debug.Log("2");
                    savedata.save.SaveDATA[i].Killcount = killcount;
                }

                if (timer > savedata.save.SaveDATA[i].Timeneeded)
                {
                    Debug.Log("3");
                    if ((i+1) != savedata.save.SaveDATA.Length)
                    {
                        savedata.save.SaveDATA[i + 1].unlocked = true;
                    }
                    
                }
            }
        }

        EndingText.text = "YOU ARE DEAD!" + "\n" + "Kill Count : " + killcount + "\n" + "Time Survived : " + (int)Mathf.Floor(timer) + " sec";

        StartCoroutine(waitForScene());
    }

    IEnumerator waitForScene()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene(0);
    }
}
