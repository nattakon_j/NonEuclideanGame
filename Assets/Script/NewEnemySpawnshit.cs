using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewEnemySpawnshit : MonoBehaviour
{
    [SerializeField] PlayeriInTrigger playerCheck;
    [SerializeField] GameObject[] Shittospawn;
    [SerializeField] Transform[] ReferencePoint;
    [SerializeField] GameObject[] RP_occupation;
    [SerializeField] GameManagerTEMP gm;
    [SerializeField] int[] UnoccupiedRP;
    [SerializeField] float spawnDelay;
    float storedspawndelay;
    float bssp;
    bool stopspawning = false;
    // Start is called before the first frame update
    void Start()
    {
        RP_occupation = new GameObject[ReferencePoint.Length];
        storedspawndelay = spawnDelay;
        bssp = spawnDelay;
        gm = GameObject.FindGameObjectWithTag("manager").GetComponent<GameManagerTEMP>();
        RPcheck();
        spawnObject();
    }

    // Update is called once per frame
    void Update()
    {
        RPcheck();
        if (!stopspawning && playerCheck._PlayerinTrigger)
        {
            if (spawnDelay <= 0)
            {
                spawnObject();
            }
            else
            {
                spawnDelay -= Time.deltaTime;
            }
            
        }
    }

    void RPcheck()
    {
        int count = 0;
        for (int i = 1; i <= RP_occupation.Length; i++)
        {
            if(RP_occupation[i-1] != null)
            {
                count++;
            }
        }   

        if(count == RP_occupation.Length)
        {
            stopspawning = true;
        }
        else
        {
            stopspawning = false;
            UnoccupiedRP = new int[RP_occupation.Length - count];
            for (int j = 0; j < UnoccupiedRP.Length; j++)
            {
                UnoccupiedRP[j] = 0;
            }
        }

        for (int j = 1; j <= UnoccupiedRP.Length; j++)
        {
            if(UnoccupiedRP[j-1] == 0)
            {
                for (int i = 1; i <= RP_occupation.Length; i++)
                {
                    if (RP_occupation[i-1] == null)
                    {
                        UnoccupiedRP[j - 1] = i;
                        break;
                    }
                }
            }
            
        }
    }

    void spawnObject()
    {
        int spawnindex = UnoccupiedRP[Random.Range(0, UnoccupiedRP.Length)] - 1;
        int shitspawn = Random.Range(0, Shittospawn.Length);
        RP_occupation[spawnindex] = Instantiate(Shittospawn[shitspawn], ReferencePoint[spawnindex].position, Quaternion.identity);
        
            if (RP_occupation[spawnindex].GetComponent<SuiscideBomber>())
            {
                RP_occupation[spawnindex].GetComponent<SuiscideBomber>().playeriInTrigger = playerCheck;

                RP_occupation[spawnindex].GetComponent<SuiscideBomber>().HP = RP_occupation[spawnindex].GetComponent<SuiscideBomber>().BHP + ((gm.killcount / 5) + ((int)Mathf.Floor(gm._timer) / 60));
            }
            else if (RP_occupation[spawnindex].GetComponent<RangeAttacker>())
            {
                RP_occupation[spawnindex].GetComponent<RangeAttacker>().playeriInTrigger = playerCheck;

                RP_occupation[spawnindex].GetComponent<RangeAttacker>().HP = RP_occupation[spawnindex].GetComponent<RangeAttacker>().BHP + ((gm.killcount / 5) + ((int)Mathf.Floor(gm._timer) / 60));
            }

        

        storedspawndelay = bssp -((gm._timer / 1000) + (gm.killcount / 100));
        if (storedspawndelay < 0.2f)
        {
            storedspawndelay = 0.2f;
        }
        spawnDelay = storedspawndelay;

    }
}
