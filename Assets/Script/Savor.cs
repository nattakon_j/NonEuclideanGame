using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Savor : MonoBehaviour
{
    public Save save;
    // Start is called before the first frame update
    void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("save");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SaveB4Exit()
    {

    }
}
