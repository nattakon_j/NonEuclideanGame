using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOndelay : MonoBehaviour
{
    [SerializeField] float Delay;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Delay -= Time.deltaTime;
        if (Delay <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
