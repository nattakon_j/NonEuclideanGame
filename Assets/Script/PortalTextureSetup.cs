using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTextureSetup : MonoBehaviour
{
    [SerializeField] Camera[] _Cam;
    [SerializeField] Material[] _Mat;

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < _Cam.Length; i++)
        {
            if (_Cam[i].targetTexture != null)
            {
                _Cam[i].targetTexture.Release();
            }

            _Cam[i].targetTexture = new RenderTexture(Screen.width, Screen.height, 24);

            _Mat[i].mainTexture = _Cam[i].targetTexture;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
