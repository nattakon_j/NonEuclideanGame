using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Basic_Enemy : MonoBehaviour
{
    [Header("Basics")]
    public int HP;
    public int BHP;
    [SerializeField] protected float MovementSpeed;
    public int damage;
    [SerializeField] protected NavMeshAgent agent;
    private Rigidbody rb;

    [Header("Detection")]
    [SerializeField] protected float Detecradius;
    public float _Detecradius => Detecradius;
    [Range(0, 360)]
    [SerializeField] protected float DetecAngle;
    
    [SerializeField] protected GameObject PlayerRef;
    public GameObject _PlayerRef => PlayerRef;
    [SerializeField] protected LayerMask Targetmask;
    [SerializeField] protected LayerMask Obstructionmask;
    [SerializeField] protected bool CanSeeplayer;
    [SerializeField] public PlayeriInTrigger playeriInTrigger;
    public bool _CanSeeplayer => CanSeeplayer;
    public float _DetecAngle => DetecAngle;
    protected GameManagerTEMP gm;
    // Start is called before the first frame update
    void Awake()
    {
        PlayerRef = GameObject.FindGameObjectWithTag("Player");
        StartCoroutine(FOVRoutine());
        rb = GetComponent<Rigidbody>();
        agent = GetComponent<NavMeshAgent>();
        agent.speed = MovementSpeed;
        gm = GameObject.FindGameObjectWithTag("manager").GetComponent<GameManagerTEMP>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void PlayerCheck()
    {
        Collider[] rangechecks = Physics.OverlapSphere(transform.position, Detecradius, Targetmask);

        if(rangechecks.Length != 0)
        {
            Transform target = rangechecks[0].transform;
            Vector3 directiontotarget = (target.position - transform.position).normalized;

            if(Vector3.Angle(transform.forward, directiontotarget) < DetecAngle / 2)
            {
                float distanceToTarget = Vector3.Distance(transform.position, target.position);

                if(!Physics.Raycast(transform.position, directiontotarget, distanceToTarget, Obstructionmask))
                {
                    CanSeeplayer = true;
                }
                else
                {
                    CanSeeplayer = false;
                }
            }
            else
            {
                CanSeeplayer = false;
            }
        } else if (CanSeeplayer)
        {
            CanSeeplayer = false;
        }
    }

    protected IEnumerator FOVRoutine()
    {
        float delay = 0.2f;
        WaitForSeconds wait = new WaitForSeconds(delay);

        while (true)
        {
            yield return wait;
            PlayerCheck();
        }
    }

    protected void death()
    {
        //PlayDeathAnim
        Destroy(this.gameObject);
        gm.killcount += 1;
    }

    public void Setdamage(int damage)
    {
        HP -= damage;
        
    }
}
