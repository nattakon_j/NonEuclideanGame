using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalCamera : MonoBehaviour
{
    [SerializeField] Transform _PlayerCam;
    [SerializeField] Transform _portal;
    [SerializeField] Transform _otherPortal;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 _PlayerOffsetFromPortal = _PlayerCam.position - _otherPortal.position;
        transform.position = _portal.position + _PlayerOffsetFromPortal;


        float _AngularDifference = Quaternion.Angle(_portal.rotation, _otherPortal.rotation);
        Quaternion _PortalrotDiff = Quaternion.AngleAxis(_AngularDifference, Vector3.up);
        Vector3 _newCamDir = _PortalrotDiff * _PlayerCam.forward;
        transform.rotation = Quaternion.LookRotation(_newCamDir, Vector3.up);
    }
}
